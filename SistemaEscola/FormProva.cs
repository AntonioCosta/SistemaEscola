﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormProva : Form
	{
		List<Prova> provas;

		public FormProva(List<Prova> provas)
		{
			InitializeComponent();
			this.provas = provas;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Prova p = new Prova();

			p.setDescricao(txtDescricao.Text);
			p.setDataProva(txtDataProva.Text);

			provas.Add(p);

			MessageBox.Show("Prova registrada com sucesso", "Sucesso", MessageBoxButtons.OK);
			limparCampos();
		}
		public void limparCampos()
		{
			txtDescricao.Text = "";
			txtDataProva.Text = "";
		}
	}
}
