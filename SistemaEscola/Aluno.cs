﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	public class Aluno
	{
		private String nome;
		private double matricula;
		private String dataNasc;
		private String dataMatricula;

		//nome
		public void setNome(String nome)
		{
			this.nome = nome;
		}
		public String getNome()
		{
			return nome;
		}
		//matricula
		public void setMatricula(double matricula)
		{
			this.matricula = matricula;
		}
		public double getMatricula()
		{
			return matricula;
		}
		//dataNasc
		public void setDataNasc(String dataNasc)
		{
			this.dataNasc = dataNasc;
		}
		public String getDataNasc()
		{
			return dataNasc;
		}
		//dataMatricula
		public void setDataMatricula(String dataMatricula)
		{
			this.dataMatricula = dataMatricula;
		}
		public String getDataMatricula()
		{
			return nome;
		}
	}
}
