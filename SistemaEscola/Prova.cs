﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	public class Prova
	{
		private String dataProva;
		private String descricao;

		//dataProva
		public void setDataProva(String dataProva)
		{
			this.dataProva = dataProva;
		}
		public String getDataProva()
		{
			return dataProva;
		}
		//descricao
		public void setDescricao(String descricao)
		{
			this.descricao = descricao;
		}
		public String getDescricao()
		{
			return descricao;
		}
	}
}
