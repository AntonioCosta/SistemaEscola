﻿namespace SistemaEscola
{
	partial class FormBuscaAluno
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtBuscarAluno = new System.Windows.Forms.TextBox();
			this.gridResultadoAluno = new System.Windows.Forms.DataGridView();
			this.gridResultado = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoAluno)).BeginInit();
			this.SuspendLayout();
			// 
			// txtBuscarAluno
			// 
			this.txtBuscarAluno.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtBuscarAluno.Location = new System.Drawing.Point(52, 26);
			this.txtBuscarAluno.Name = "txtBuscarAluno";
			this.txtBuscarAluno.Size = new System.Drawing.Size(241, 20);
			this.txtBuscarAluno.TabIndex = 0;
			this.txtBuscarAluno.TextChanged += new System.EventHandler(this.txtBuscarAluno_TextChanged);
			// 
			// gridResultadoAluno
			// 
			this.gridResultadoAluno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridResultadoAluno.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridResultado});
			this.gridResultadoAluno.Location = new System.Drawing.Point(2, 111);
			this.gridResultadoAluno.Name = "gridResultadoAluno";
			this.gridResultadoAluno.Size = new System.Drawing.Size(350, 211);
			this.gridResultadoAluno.TabIndex = 1;
			this.gridResultadoAluno.Visible = false;
			this.gridResultadoAluno.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridResultadoAluno_CellContentClick);
			// 
			// gridResultado
			// 
			this.gridResultado.HeaderText = "Resultados da Busca:";
			this.gridResultado.Name = "gridResultado";
			this.gridResultado.Width = 305;
			// 
			// FormBuscaAluno
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(352, 325);
			this.Controls.Add(this.gridResultadoAluno);
			this.Controls.Add(this.txtBuscarAluno);
			this.Name = "FormBuscaAluno";
			this.Text = "FormBuscaAluno";
			this.Load += new System.EventHandler(this.FormBuscaAluno_Load);
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoAluno)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtBuscarAluno;
		private System.Windows.Forms.DataGridView gridResultadoAluno;
		private System.Windows.Forms.DataGridViewTextBoxColumn gridResultado;
	}
}