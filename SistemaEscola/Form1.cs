﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormPrincipal : Form
	{
		List<Aluno> alunos;
		List<Prova> provas;
		List<Nota> notas;

		public FormPrincipal()
		{
			InitializeComponent();
			alunos = new List<Aluno>();
			provas = new List<Prova>();
			notas = new List<Nota>();
		}

		private void novoAlunoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormAluno formAluno = new FormAluno(alunos);
			formAluno.Show();
		}

		private void cadastrarProvaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormProva formProva = new FormProva(provas);
			formProva.Show();
		}

		private void registarNotaToolStripMenuItem_Click(object sender, EventArgs e)
		{        // coloquei alunos no lugar de notas
			FormNota formNota = new FormNota(alunos, provas, notas);
			formNota.Show();
		}

		private void FormPrincipal_Load(object sender, EventArgs e)
		{

		}

		private void btBuscaAluno_Click(object sender, EventArgs e)
		{
			List<Nota> notasBusca = new List<Nota>();

			foreach (Nota n in notas)
			{
				if (n.getAluno().getNome().Contains(txtBuscaAluno.Text))
				{
					notasBusca.Add(n);
				}
			}

			if (notasBusca.Count() == 0)
			{
				lblNenhumRegistro.Visible = true;
				gridAluno.Visible = false;
			}

			else
			{
				gridAluno.Visible = true;
				lblNenhumRegistro.Visible = false;
			}

			foreach (Nota n in notasBusca)
			{
				gridAluno.Rows.Add(n.getAluno().getNome(), n.getProva().getDescricao(), n.getNota());
			}
		}

		private void btLimparBusca_Click(object sender, EventArgs e)
		{
			txtBuscaAluno.Text = "";
			gridAluno.Rows.Clear();
			lblNenhumRegistro.Visible = false;
		}

		private void buscarAlunoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			groupBoxAlunos.Visible = true;
			btVoltar.Visible = true;
			menuStrip1.Visible = false;
		}

		private void btVoltar_Click(object sender, EventArgs e)
		{
			groupBoxAlunos.Visible = false;
			btVoltar.Visible = false;
			menuStrip1.Visible = true;
		}
	}
}
