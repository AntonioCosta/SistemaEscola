﻿namespace SistemaEscola
{
	partial class FormBuscaProva
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gridResultadoProva = new System.Windows.Forms.DataGridView();
			this.gridResultado = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtBuscarProva = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoProva)).BeginInit();
			this.SuspendLayout();
			// 
			// gridResultadoProva
			// 
			this.gridResultadoProva.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridResultadoProva.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridResultado});
			this.gridResultadoProva.Location = new System.Drawing.Point(1, 102);
			this.gridResultadoProva.Name = "gridResultadoProva";
			this.gridResultadoProva.Size = new System.Drawing.Size(350, 211);
			this.gridResultadoProva.TabIndex = 3;
			this.gridResultadoProva.Visible = false;
			this.gridResultadoProva.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridResultadoProva_CellContentClick);
			// 
			// gridResultado
			// 
			this.gridResultado.HeaderText = "Resultados da Busca:";
			this.gridResultado.Name = "gridResultado";
			this.gridResultado.Width = 305;
			// 
			// txtBuscarProva
			// 
			this.txtBuscarProva.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtBuscarProva.Location = new System.Drawing.Point(51, 17);
			this.txtBuscarProva.Name = "txtBuscarProva";
			this.txtBuscarProva.Size = new System.Drawing.Size(241, 20);
			this.txtBuscarProva.TabIndex = 2;
			this.txtBuscarProva.TextChanged += new System.EventHandler(this.txtBuscarProva_TextChanged);
			// 
			// FormBuscaProva
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(352, 314);
			this.Controls.Add(this.gridResultadoProva);
			this.Controls.Add(this.txtBuscarProva);
			this.Name = "FormBuscaProva";
			this.Text = "FormBuscaProva";
			this.Load += new System.EventHandler(this.FormBuscaProva_Load);
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoProva)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView gridResultadoProva;
		private System.Windows.Forms.TextBox txtBuscarProva;
		private System.Windows.Forms.DataGridViewTextBoxColumn gridResultado;
	}
}